import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Add Schedule",
    home: AddSchedule(),
  ));
}

class AddSchedule extends StatelessWidget {
  static const routeName = '/add-schedule';

  // const AddSchedule ({Key? key}) : super(key: key);
  const AddSchedule () : super();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Schedule Form',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Schedule Form"),
        ),
        body: ScheduleForm(),
      ),
    );
  }

}

// Schedule Form Widget
class ScheduleForm extends StatefulWidget {

  @override
  MyCustomFormState createState() => MyCustomFormState();

}

// class yang memiliki data yang berhubungan dengan form
class MyCustomFormState extends State<ScheduleForm> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child:Text('Add Schedule in here.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.blueAccent)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Name: '),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:TextFormField(
                  autofocus: true,
                  decoration: new InputDecoration(
                      hintText: "Enter a schedule title",
                      labelText: "Name of schedule",
                      icon: Icon(Icons.assignment),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                      )
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add a valid text for name';
                    }
                    return null;
                  },
                ),
          ),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Day: ')
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:TextFormField(
                decoration: new InputDecoration(
                    hintText: "Enter a day of schedule",
                    labelText: "Day of schedule",
                    icon: Icon(Icons.calendar_today),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)
                    )
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please add a valid day';
                  }
                  return null;
                },
              )
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Start Time: ')
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:TextFormField(
                  keyboardType: TextInputType.datetime,
                  decoration: new InputDecoration(
                      hintText: "Enter a start time of schedule",
                      labelText: "Start Time of schedule",
                      icon: Icon(Icons.watch_later),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                      )
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add a valid time';
                    }
                    return null;
                  },
                )
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Due Time: ')
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:TextFormField(
                  keyboardType: TextInputType.datetime,
                  decoration: new InputDecoration(
                      hintText: "Enter a end time of schedule",
                      labelText: "End Time of schedule",
                      icon: Icon(Icons.watch_later_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                      )
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add a valid time';
                    }
                    return null;
                  },
                )
          ),
          Padding(
            // padding: const EdgeInsets.symmetric(vertical: 16.0),
            padding: EdgeInsets.all(20.0),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  // if the form is valid, display a snackbar
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Add Schedule Success')),
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}