from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login') # Enable form only for authenticated user
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login') # Enable form only for authenticated user
def add_friend(request):
    context = {}

    # create object of form
    form = FriendForm(request.POST or None)

    # check if form data is valid and request method is POST
    if (form.is_valid and request.method == 'POST'): # validating form data
        form.save() # if form is valid, save that data
        return HttpResponseRedirect('/lab-3') # redirect to /lab-3
        # return redirect('/lab-3')
    
    context['form'] = form
    return render(request, "lab3_form.html", context)