from django.forms import ModelForm, DateTimeInput
from lab_1.models import Friend

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"

        # specify input type for 'date of birth' menjadi date
        widgets = { 
            'date_of_birth': DateTimeInput(attrs={'type': 'date'}),
        }
