from django.db import models
from django.utils import timezone
from datetime import date, time

class Friend(models.Model):
    name = models.CharField(max_length=30) 
    NPM = models.CharField(max_length=10)
    date_of_birth = models.DateField()
