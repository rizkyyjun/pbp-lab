from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)

    if (form.is_valid and request.method == 'POST'): # cek apakah form valid dan request methodnya POST
        form.save() # if valid, save it
        return HttpResponseRedirect('/lab-4') # redirect to /lab-4
    
    response = {'form': form}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)


