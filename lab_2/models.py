from django.db import models
from django.utils import timezone
from datetime import date, time

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    title = models.CharField(max_length=30) 
    message = models.TextField(blank=True)
