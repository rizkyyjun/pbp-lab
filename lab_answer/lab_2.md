# Apakah perbedaan antara JSON dan XML?
JSON:
1. Sesuai dengan namanya, JavaScript Object Notation, syntax yang digunakan oleh JSON mirip dengan Javascript
2. Format yang digunakan berupa data interchange
3. Data oriented
4. Mendukung penggunaan array
5. Hanya mendukung tipe data teks dan numerik
6. Lebih sederhana dan mudah untuk dibaca oleh manusia
7. Digunakan untuk pertukaran data ringan yang datanya lebih mudah diurai oleh komputer

XML:
1. Syntax yang digunakan mirip seperti HTML, menggunakan tag-tag(pembuka dan penutup), tetapi tag pada XML bisa dibuat (custom) sendiri oleh ownernya (dapat dikembangkan tagnya)
2. Format yang digunakan berupa markup language
3. Document oriented
4. Tidak mendukung penggunaan array secara langsung, jika ingin menggunakan array harus menambahkan tag untuk tiap item
5. Mendukung berbagai tipe data, seperti teks, numerik, gambar, grafik, dll
6. Tidak selalu sederhana dan mudah untuk dibaca oleh manusia
7. Digunakan untuk menyimpan dan mengangkut data dari satu aplikasi ke aplikasi lain melalui internet yang mengandung informasi metadata


# Apakah perbedaan antara HTML dan XML?
HTML:
1. Syntaxnya menggunakan tag-tag(pembuka dan penutup), tetapi nama untuk tiap tag tidak bisa dibuat sendiri, telah ditentukan sebelumnya
2. Menitikberatkan pada bagaimana format tampilan dari data
3. Tersusun atas tag-tag yang mengatur bagaimana data dalam file itu akan ditampilkan, tetapi tidak ada informasi mengenai isi dari data tersebut
4. Berfokus pada penyajian data
5. Sifatnya case insensitive
6. Tidak menyediakan dukungan namespaces

XML:
1. Syntaxnya menggunakan tag-tag(pembuka dan penutup) dan nama untuk tiap tag bisa dibuat sendiri (di-custom) oleh ownernya (dapat dikembangkan tag nya)
2. Menitikberatkan pada struktur dan konteksnya
3. Tersusun atas tag-tag yang mengatur bagaimana data dalam file itu akan ditampilkan serta ada informasi mengenai isi dari data tersebut dalam bentuk format yang terstruktur
4. Berfokus pada transfer data
5. Sifatnya case sensitive
6. Menyediakan dukungan namespaces